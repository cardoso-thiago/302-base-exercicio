pipeline {
    agent any
    parameters{
        string(name: "DEPLOY_PATH", defaultValue: "/home/ubuntu", description: "Caminho para o deploy")
        string(name: "MACHINE_USER", defaultValue: "ubuntu", description: "Usuário da máquina")
        string(name: "MACHINE_ADDRESS", defaultValue: "3.20.237.216", description: "Endereço da máquina")
        string(name: "SERVICE_NAME", defaultValue: "investimentos", description: "Nome do serviço na máquina remota")
    }
    post {
        success {
            echo 'O build foi realizado com sucesso.'
            updateGitlabCommitStatus name: "build", state: "success"
        }
        failure {
            updateGitlabCommitStatus name: "build", state: "failed"
            echo 'O build falhou, verifique o log para realizar as correções.'
        }
        aborted {
            updateGitlabCommitStatus name: "build", state: "canceled"
            echo 'O build foi cancelado.'
        }
        always {
            echo 'Limpando pastas temporárias da esteira.'
            cleanWs()
            echo 'Fim do processo de build.'
        }
    }
    options {
        gitLabConnection("gitlab")
    }
    stages {
        stage('Java Version') {
            steps {
                updateGitlabCommitStatus name: "build", state: "running"
                echo 'Verificando a versão do java.'
                sh "java -version"
            }
        }
        stage('Clean') {
            steps {
                echo 'Limpando o projeto'
                sh "./mvnw clean"
            }
        }
        stage('Tests') {
            steps {
                echo 'Início do bloco de testes.'
                sh "./mvnw test"
            }
        }
        stage('Package Master') {
            when {branch 'master'}
            steps {
                echo 'Gerando o pacote da branch master.'
                sh "./mvnw package -DskipTests"
            }
        }
        stage("Package Generic Branch") {
            when { not {branch 'master'}}
            steps {
                echo "Gerando o pacote da branch ${BRANCH_NAME}"
                sh "./mvnw package -DskipTests"
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploy do pacote.'
                script {
                    def jar = sh(script: 'find target/ -name Api-Investimentos*.jar', returnStdout: true).trim()
                    sh "scp -o StrictHostKeyChecking=no $jar ${params.MACHINE_USER}@${params.MACHINE_ADDRESS}:${params.DEPLOY_PATH}"
                }
            }
        }
        stage("Service Restart"){
            steps{
                sh "ssh -o StrictHostKeyChecking=no -T ${params.MACHINE_USER}@${params.MACHINE_ADDRESS} sudo systemctl restart ${params.SERVICE_NAME}"
            }
        }
        stage("Service Check"){
            steps{
                script {
                    def statusCode = pingService()
                    echo "Retorno da chamada inicial: ${statusCode}"

                    def count = 1
                    while(statusCode == "502" && count <= 3) {
                        sleep(15)
                        statusCode = pingService()
                        echo "Retorno atual ${statusCode}, tentativa ${count}"
                        count++
                    }
                    if(statusCode != "200") {
                        error("Build falhou, o serviço retornou ${statusCode}")
                    }
                }
            }
        }
    }
}

def pingService()
{
   def statusCode = sh (
       script: "curl -o -I -L -s -w '%{http_code}' http://${params.MACHINE_ADDRESS}/investimentos",
       returnStdout: true
       ).trim()
    return statusCode
}
